
# Contributing Guidelines

Here are the steps to contribute to this repository:  
1. Create an account for [Codeberg](https://codeberg.org)
1. Fork this repository.
1. Clone your fork onto your local system.
1. Create a branch in your local repository.
1. Update your local copy of the repository.
1. Push your changes to your remote fork.
1. Issue a pull request to the main repository by using the "New Pull Request"
   button at the top of your fork's view in the browser, or by the command line
   (instructions not provided here).
1. The pull request will be reviewed by the
   [leadership committee](LEADERSHIP.md) of this branch of the Shlaer-Mellor
   Commons. Review comments will be placed in the pull request comment area.
   A rejection of the pull request will have the reason stated.
1. After all review comments are addressed, the pull request will be merged by
   one of the leadership committee.

Here are the steps to keep your fork current with this repository:  
1. Add this repository as a remote named, "upstream".  
   e.g., `git remote add upstream <repository link>`
1. Run  
    `git fetch upstream`  
    `git merge upstream/main`  
    `git push origin HEAD`

For support, contact one of the [leadership committee](LEADERSHIP.md).

